---
title: 'Notes for KWLUG March 2016: DVD Creation and KDEnlive'
tags: kwlug
date: 2016-04-24
---

# [Raul Suarez](https://twitter.com/rarsamx): Creating a Photo DVD
  * Raul is using [FreeMind](http://freemind.sourceforge.net/wiki/index.php?Download)
    to organize his presentation, as usual
  * goals
    - open source
    - easy to teach
    - burn photos and videos to DVD
  * did the work over a weekend
  * video came on an [SD card](https://en.wikipedia.org/wiki/Secure_Digital) in [MTS format](https://en.wikipedia.org/wiki/AVCHD)
  * plan to spend time curating and making the photo orientation consistent
  * options considered
    - [ffmpeg](https://ffmpeg.org/)
    - [dvd-slideshow](http://dvd-slideshow.sourceforge.net/wiki/Main_Page)
    - [slcreator](http://slcreator.sourceforge.net/)
    - [KDEnlive](https://kdenlive.org/)
    - [2manDVD](http://2mandvd.tuxfamily.org/website/)
    - [Brasero](https://wiki.gnome.org/Apps/Brasero/)
  * options chosen
    - [Imagination](http://imagination.sourceforge.net/) - slideshow
    - [DeVeDe](http://www.rastersoft.com/programas/devede.html) - DVD writer
  * Raul found these were both straight forward and quickly did what he wanted
    - he did not need any tutorials for either tool
  * there were 3 different events recorded and he wanted chapter/section for easy navigation
  * it was easy to import some photos to play with, and display pretty easily
  * background music can be added as well, but this does not play in preview mode
    - using a long piece of music for the menu actually generates a video
    - choosing a short piece of music will make this smaller and faster
  * doing the [VOB](https://en.wikipedia.org/wiki/VOB) generation is in "go and have dinner" time range
  * by generating an [ISO image](https://en.wikipedia.org/wiki/ISO_image) so that his father
    could download and burn avoiding puttig physical media into the postal service
  * creating the ISO can be another "go and have dinner" wait
  * mount the ISO to preview
    - [Bob](http://sobac.com/sobac/) mentioned that [VLC](https://www.videolan.org/vlc/) might also have an option for playing ISO's directly
  * Raul's final product for 2 hours was ~40% of the DVD, using [SD video](https://en.wikipedia.org/wiki/Standard-definition_television)

# [Bob Jonkman](http://sobac.com/sobac/): Video Editing with [KDEnlive](https://kdenlive.org/)
  * Bob is going to try to use video that he recorded from Raul's presentation for the demo
  * [Cinelara](http://cinelerra.org/2015/) is a more capable than [KDEnlive](https://kdenlive.org/), but
    also more complicated
  * his install tends to crash on transitions so he will avoid them
  * using [Saw Square Noise](http://rolemusic.sawsquarenoise.com/) music for
    the sound track, which is licensed [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/deed.en_US)
  * Bob started this while making a nice video of his [GNU social presentation](http://kwlug.org/node/993)
  * would like the razor tool to disable itself after use, as he usual want to do one cut at a time
  * the edits are stored in XML and the source videos are not changed
  * can separate the video & audio good for things like combining and cutting between multiple cameras
  * 3 - 4 times as long as the actual video to generate the whole video
  * Bob has not gotten into the back-end details
  * can also do a slideshow clip option
  * can normalize audio
  * lots of fancy 3d options
  * through this project Bob has gained more appreciation of how hard video/film editing actually is
