andrewcant.ca
=============

This is the [Middleman](http://middlemanapp.com) powered repository for [andrewsullivancant.ca](http://www.andrewcant.ca)

[Backlog](backlog)

To Deploy
---------
~~~
bundle exec middleman deploy
~~~

Framework Requirements
----------------------
Notes about choosing which framework to use to building my homepage. This will
be useful to comparing new frameworks to decide at what point I might want to
switch. The current options are:
* [Middleman](http://middlemanapp.com/)
* [Jekyll v2](http://jekyllrb.com/)
* [Octopress](http://octopress.org/docs/) (but the released version has not be updated to Jekyll v2 as of
  2014-08-17)

Requirements:
* static generator
* FLOSS (GPL > MIT > other)
* Ruby is preferred
* HAML, SASS (through compass)
* health project (relatively popular, good contribution base, good code
  quality)
* already used by other for indieweb style web sites

Design Ideas
------------
* http://www.tutorialchip.com/inspiration/best-and-creative-personal-websites/
* http://sixrevisions.com/design-showcase-inspiration/responsive-webdesign-examples/
* http://socialdriver.com/2013/06/10/50-best-responsive-website-design-examples-of-2013/
* http://fizzle.co/sparkline/personal-domain-names
* http://everypageispageone.com/

Other nice looking personal sites:
* http://www.brainshave.com/
* http://tobyx.com/
* http://fmarier.org/
  - this is really nice and simple
  - and the author is in Debian, UW alumni, and works for Mozilla
* http://garretkeizer.com/
* http://spin.atomicobject.com/2015/08/24/learn-ember-js-quickly/
  - moves the link bar to the right in the widest mode
* http://danluu.com/
* https://gyrosco.pe/ and http://aprilzero.com/
* https://adactio.com/
* https://sarasoueidan.com/
* http://sarahdrasnerdesign.com/
* https://www.johanbrook.com/
* http://davidmiranda.info/
* https://mzucker.github.io/
* http://kellysutton.com/
* http://victoriablessing.com/
* https://www.igvita.com/
* https://sonniesedge.co.uk/
* https://mylesb.ca/

SEO Guidelines
--------------
* http://katemats.com/what-every-programmer-should-know-about-seo/
