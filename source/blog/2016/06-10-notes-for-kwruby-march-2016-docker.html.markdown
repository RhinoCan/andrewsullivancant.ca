---
:title: 'Notes for KWRuby March 2016: Docker'
:tags: kwruby docker
:date: 2016-06-10
---
[Tim Laurence](https://ca.linkedin.com/in/timlaurence) presented to [KWRuby](http://kwruby.ca)
on [March 15 2016](http://www.meetup.com/kw-ruby-on-rails/events/228654199/).
He took us through a workshop on how to use the [Docker](https://www.docker.com/)
system for managing [containers](https://en.wikipedia.org/wiki/LXC).

Tim has also done some other related presentations:

* he talked more about the technical details of docker at [KWLUG's February 2015 meeting](http://kwlug.org/node/948)
* he will also repeat the talk at [KWLUG in April](http://kwlug.org/node/1017)

And finally the notes for this presentation:

* Tim has a CS background and has been working as a sysadmin for ~20 years
* he has coming backing into some developer stuff recently, working mostly in
  python
* Docker containers are composed of layered file system images
  - these are layered together to get the final operating system
  - layers can removed (i.e., block out) things from previous layers
  - layers are addressed by hash so they cannot change without notice
* when running a container changes occur in the top layer, which is read-write
* you can list all the layers and their total size

```
docker images
```

* [hub.docker.com](http://hub.docker.com) is the default place to find and publish docker images
  - it is also possible to store a private repository for private images or for local caching
  - anyone can push and images into hub.docker.com
  - so there are official images and un-official ones
    * looks sort of like projects on [github](http://github.com)
* docker log returns to the STDOUT or STDERR
* can turn a container into and image
  - tim is not going to show that but you can do it if you absolutely need to
* list the contains which are running

```
docker ps
```

* list all the contains, of any status

```
docker ps -a
```

* can mount host directories in the docker container, either Read-Write or ReadOnly
* docker has recently added a [plugin system](https://blog.docker.com/2015/06/extending-docker-with-plugins/), examples of plugins include:
  - making two systems appears local to each other
  - use other external storage systems (e.g., NAS type stuff)
* can mount a simple directory with **docker volume** for local data
* there are various ways to handle secrets, but no canonical way to do it
* images can be created by using a Dockerfile
  - https://docs.docker.com/engine/reference/builder/
  - some Dockerfile commands create new, intermediate, images
  - other commands are run at build time
    * **CMD** this is run when the image is started
    * **ENTRYPOINT** is similar, but Tim is not discussing
* [docker-compose](http://docs.docker.com/compose/)
  - will run and coordinate various docker machines
  - it seems very similar to a [Vagrant](http://vagrantup.com) file
