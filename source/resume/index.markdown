---
title: ASC Resume
---

Please find my resume in other formats at these links: [PDF](resume/Andrew_Sullivan_Cant_resume.pdf), [OpenDocument](resume/Andrew_Sullivan_Cant_resume.odt)

Andrew Sullivan Cant  
Kitchener, Ontario  
[acant@alumni.uwaterloo.ca](mailto:acant@alumni.uwaterloo.ca)

Profile
=======
* 12 years experience designing and implementing software solutions
* Over 4 years experience in billing and network traffic management
* Experience in project management and customer relations
* Supervised software development teams
* Coordinated software development across teams
* Assessed, integrated, and built upon open source software to meet business needs
* Participated in Agile and Scrum development in various industries
* Conscientious and dedicated to quality

Technical Skills Summary
========================
* Experienced in various languages, including Ruby, Perl, PHP, C/C++, Java, C#, and Bash
* Proficient in Windows and Unix/Linux (Debian, RedHat, MacOSX, Solaris) environments
* Proficient with project management software (such as, make, rake, capistrano, ant, git, Maven) and unit testing frameworks within Java, C# and Ruby
* Experienced in Linux configuration, general and network-related
* Proficient in software engineering techniques (such as, design patterns, refactoring, Agile and Scrum)
* Produced quality documentation using various desktop publishing packages and mark-up languages (HTML, LaTeX, XML, MSWord, OpenOffice)
* Designed and produced websites using JavaScript, jQuery, Ruby On Rails, Drupal, PHP, MonoRail, C#, Apache web server and W3C XML-based standards
* Created and maintained complex relational databases using MySql, PostgreSQL, MSSQL, and Oracle
* Created and maintained internal wikis using Dokuwiki and Mediawiki
* Configured and administered a Nagios-based network monitoring and reporting system
* Created a web-based mapping application based upon OpenStreetMap data, OpenLayers, Mapnik and PostGIS
* Created a web-based medical image management application based upon Ruby on Rails and JRuby, which communicated with a Java-based backend
* Experienced in database configuration and SOAP integration with Microsoft Dynamics CRM
* Experienced in SQL level integration with Microsoft Dynamics GP

Work Experience
===============

Product Developer, [Karos Health Limited](http://karoshealth.com) (2011 - 2012)
---------------------------------------------------
* Designed, built, and successfully deployed a web-based user interface to transport and manage medical images, such as x-rays 
* Used Ruby on Rails, JRuby, JQuery, and RSpec to build the web-based user interface
* Tested, maintained, and enhanced the Java-based backend of the web-based user interface
* Implemented and tested medical protocols and profiles related to the Integrating the Healthcare Enterprise (IHE) IT Infrastructure (ITI), such as DICOM, HL7, XDS, and PIX/PDQ
* Installed software remotely on customers' servers and ensured its success
* Mentored and trained employees and co-op students in software development, design, testing, deployment, and QA
* Co-created SimSite, an internal application that developers use to generate test data for HL7 and XDS protocols
* Participated in the Scrum planning process
* Used Agile programming methodologies, including Test Driven Development, Pair Programming, and Continuous Integration
* Used git, Eclipse, Maven, Jira, and Hudson/Jenkins software development tools
* Built and deployed software using Red Hat Enterprise Linux (RHEL) and CentOS systems
* Evaluated and integrated existing open source projects into the company's software products

Software Developer/Scrum Master, [Demeure](http://demeure.com) (2010 - 2011)
----------------------------------------------------------------
* Designed, built and maintained a vacation property rental management system
* Helped to evaluated the requirements for the system based upon existing and planned operations
* Used Ruby On Rails to build the public web presence, member portal, and backend supporting software
* Used the Amazon Elastic Computing environment for server and database hosting
* Integrated and configured SalesForces to work with the frontend web systems
* Participated in Scrum development process as both developer and Scrum Master
* Interviewed, supported, coached, and evaluated newly hired developers


Software Developer, [Everus Communications Inc.](http://www.everus.ca) (2008 - 2010)
----------------------------------------------------------------------------------------
* Used an Agile development process to discover requirements and deliver software to match those requirements
	* Gathered requirements
	* Designed and developed software in short iterations
	* Incorporated user feedback
* Surveyed and selected software to integrate with existing systems independently
* Facilitated monthly meetings to introduce new software features and gather feedback
* Increased developer productivity by creating an internal wiki that centralized documentation
* Increased staff productivity and improved data quality using an interface design that reduced duplicate entry and increased visible customer information
* Reduced response time for the resolution of network issues by performing the following:
	* Improved network monitoring systems
	* Enabled visualization of the geographic location and status of network infrastructure

Developer/Customer Integrations Project Manager, [LogiSense Corporation](http://www.logisense.com) (2004 - 2008)
----------------------------------------------------------------------------------------------------------------
* Maintained and added features for a Linux-based traffic management system for IP networks
* Maintained and added features for a cross-platform (Linux,Windows) high-performance web-cache
* Maintained and added features for a telecom billing system
* Designed two systems for provisioning third-party hardware and software
* Integrated billing systems with third-party systems
* Created integration and unit tests for traffic management and billing systems
* Managed a team of two developers
* Liaised with clients in problem-solving their special needs

Database Developer, [Princess Margaret Hospital](http://www.uhn.ca/pmh/) (2001 - 2002)
--------------------------------------------------------------------------------------
* Designed, implemented and maintained a database using Oracle and Perl
* Created Perl scripts that generated quarterly reports and transmitted them via FTP
* Produced a web-based interface for data-entry, using PHP
* Gathering and analyzed requirements data in a team-setting

Education
=========
Bachelor of Mathematics, Co-operative Honours Computer Science, Software Engineering Option, [University of Waterloo](http://www.uwaterloo.ca), Waterloo, Ontario, 2003

Interests
=========
* Baking and cooking simple, delicious vegetarian meals
* Advocating Open Source Software projects

Membership
==========
* [Kitchener-Waterloo Linux User's Group](http://kwlug.org)
