---
title: 'FLOSS Minecraft'
# date: TBD When publishing
tags: floss free_software minecraft
---

# FLOSS Minecraft

[MineTest](https://www.minetest.net/#features)
[TrueCraft](https://truecraft.io/)

## List of alternatives both FLOSS and not

* https://alternativeto.net/software/minecraft/
* https://opensource.com/alternatives/minecraft
