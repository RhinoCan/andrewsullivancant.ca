---
:title: 'Notes for KWLUG June 2016: RaspberryPi'
:tags: kwlug raspberry_pi
:date: 2016-07-13
---
Omar and [Khalid Baheyeldin](http://2bits.com) presenting about using the
[RaspberryPi](https://www.raspberrypi.org/) at [KWLUG](http://kwlug.org/) on [June 6 2016](http://kwlug.org/node/1026). Omar described his Pi based plant watering project, and Khalid reviewed the new [RaspberryPi 3](https://www.raspberrypi.org/products/raspberry-pi-3-model-b/)

# Omar presents his Automated Plant Watering project
* built a automated planter watering project as part of his schools science
  fair
* moisture sensor + [MCP3008](https://www.adafruit.com/products/856) [ADC](https://en.wikipedia.org/wiki/Analog-to-digital_converter) to connection to the [GPIO](https://en.wikipedia.org/wiki/Gpio) header
* consider using a monitoring/controlling services
  - but the data would be sent by email, which was not responsive enough
  - and connecting to the service would not work through his schools firewall
* Omar also needed to supply his only wireless network, as the school's network
  would not pass his traffic, and we were able to connect to his router to see
  the graphs the device was generating
* used an LED and an electro-relay to demonstrate how the valve would be
  controlled
* demonstrated the data collection just by touching the moisture sensor
* software used
  - [python](https://www.python.org/)
  - pygraph
  - [bottle web server](http://bottlepy.org/docs/dev/index.html)
  - [SQLite](https://sqlite.org/)

# Khalid's Review of the RaspberryPi 3
* using the CanaKit Ultimate starter kit
* [RaspberryPi 2](https://www.raspberrypi.org/products/raspberry-pi-2-model-b/), which is supported in [Debian](https://www.debian.org/)
* [RaspberryPi 3](https://www.raspberrypi.org/products/raspberry-pi-3-model-b/), now [64-bit](https://en.wikipedia.org/wiki/64-bit_computing) and has integrated [WiFi]() and [Bluetooth LE]()
* RaspberryPi 3 still has proprietary boot loader, network and GPU
* Khalid uses [Raspian](https://www.raspbian.org/) but there are lost of other OSes to choose from
* Weather Forecaster project
  - bottle web server
  - [pywapi](https://code.google.com/archive/p/python-weather-api/), and he had to fix a bug in the process
* Internet Radio
  - [mopidy](https://www.mopidy.com/)
    * can control through a variety of clients (e.g., [ncmpc](https://musicpd.org/clients/ncmpc/), [mpdroid](https://play.google.com/store/apps/details?id=com.namelessdev.mpdroid)
    * tried an IR remote but the one he bought would not work
  - [kodi](https://kodi.tv/), formerly XMBC
    * not headless, need a screen to navigate
    * but one a stream is selected you can start and stop without looking
* investigating the [OpenHab](http://www.openhab.org/) home automation project
