---
title: Recipes
---

* [BBQ Tofu](/wiki/bbq_tofu)
* [Cream of Wheat](/wiki/cream_of_wheat)
* [Crustless Quiche](/wiki/crustless_quiche)
* [Dahl](/wiki/dahl)
* [Lentil Burger](/wiki/lentil_burger)
