---
title: Crustless Quiche
---

* 4 eggs
* 1 cup milk
* 1 tsp baking powder
* 1/4 tsp salt + pepper
* 1 tbsp some herb
* 1 medium onion
* 1/2 cup cheese, grated
* 4 heaping tbsp flour
* some, or all, of the following:
	- 1 grated carrot
	- 1/4 broccoli head, chopped
	- 3 kale leaves, chopped
	- 2 cans of salmon

Instructions
============
1. Pre-heat oven to 350C
2. Start sautéing the onions, then add the remaining vegetables
3. In a medium bowl beat the eggs
4. Add milk, flour, baking powder, salt, pepper and herb. Wisk everything
   together
5. Add the vegetables, fish, and grated cheese
6. Butter a 9" pie plate
7. Pour the mixture into the pie plate
8. Cook for 35 minutes, and the bottom of the pie is browned and a knife
   will come out dry when inserted into the middle

**Source:** Originally an Impossible Pie recipe from the back of a [Bisquick](http://www.bettycrocker.com/products/bisquick) box
