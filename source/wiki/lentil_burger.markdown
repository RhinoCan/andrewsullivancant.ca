---
title: Lentil Burger
---

* 1 grated onion
* 3/4 cups wheat germ
* 2 cup cooked lentils or 1-19oz canned lentils
* 1 cup bread crumbs
* 3 tbsp olive oil
* 1/2 tsp salt
* 1/2 tsp pepper

Instructions
============

1. Set aside 2 tbsp of the wheat germ
2. Stir all ingredients together in a medium bowl
3. Shape into 8 patties
4. Before cooking cover each patty in the reserved wheat germ
5. Cook on a lightly oiled pan on medium heat for 5 - 10 minutes, flipping once

**Source:** Original recipe from [How It All Vegan!](https://openlibrary.org/works/OL15172613W/How_It_All_Vegan!)
