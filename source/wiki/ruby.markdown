---
title: Ruby
---

Various notes, links, and information about the [Ruby programming language](https://www.ruby-lang.org/en/).

Feature Flags
=============
[Feature flags](https://en.wikipedia.org/wiki/Feature_toggle) can be used to enable and
disable code dynamically. This makes it easier to do [Continuous Deployment](https://en.wikipedia.org/wiki/Continuous_delivery) since a new feature can be enabled in a limited manner.

There are a variety of gems which implement features flags in various ways:

* https://github.com/jamesgolick/rollout
* https://github.com/pda/flip
* https://github.com/pandurang90/feature_flags
* https://github.com/gmontard/helioth
* https://github.com/ckdake/setler
* https://github.com/MongoHQ/mongoid-feature-flags
* https://github.com/grillpanda/dolphin
* https://github.com/qype/feature_flipper

Sources and Discussion
----------------------
* http://stackoverflow.com/questions/4995556/ruby-feature-switches-feature-flippers
* http://www.stakelon.com/2012/01/simple-feature-flags-for-rails-mongoid/

Invariants and Assertions
=========================
[Software Assertions](https://en.wikipedia.org/wiki/Assertion_(software_development)) are statements in a routine which should always be true. There statements may or may not be disabled in production.

A simple version of this could be implemented just by raising exceptions.
However there are gems which allow for better control and make the difference between exceptions and assertions more clear.

* https://github.com/pithyless/invariant
* https://github.com/jorgemanrubia/solid_assert

Building Gems
=============
Guidelines for building/maintaining gem projects

* manage with [bundler](http://bundler.io/)
* choose a license
  - [GPL](https://www.gnu.org/licenses/gpl.html)
  - [MIT](https://en.wikipedia.org/wiki/MIT_License)
* setup support services and add badges to README
  - [RubyGems](http://rubygems.org/) (released version)
  - [CodeClimate](https://codeclimate.com) (code quality and test coverage)
  - [TravisCI](https://travis-ci.org) (continuous integration)
  - [InchCI](http://inch-ci.org) (documentation quality)
  - [Gemnasium](https://gemnasium.com) (dependency checking)
* gems to use by default:
  - [thor](http://whatisthor.com/) for CLI
  - [excon](https://github.com/excon/excon) for HTTP client
  - [rspec](http://rspec.info/) and [simplecov](https://github.com/colszowka/simplecov) for unit testing
  - [cucumber](https://cucumber.io/) for documentation and integration tests
  - [rubocop](https://github.com/bbatsov/rubocop) for Ruby static analysis
  - [capybara](https://teamcapybara.github.io/capybara/) for web browser testing
  - [webmock](https://github.com/webmock/webmock)
  - [fakefs](https://github.com/fakefs/fakefs)
  - [faker](https://github.com/stympy/faker)
  - [MulitJson](https://github.com/intridea/multi_json) + [oj](http://www.ohler.com/oj/)
  - [bundler-audit](https://rubygems.org/gems/bundler-audit)
  - [license_finder](https://rubygems.org/gems/license_finder)
* sign the published gems
* publish the project code and related information
  - [Github](http://github.com/)
  - [GitLab](http://gitlab.com/)
  - [Ruby Gems](http://rubygems.org/)
  - [Ruby Toolbox](http://rubytoolbox.org)
  - [OpenHub](https://www.openhub.net/)
  - [Ruby LibHunt](https://ruby.libhunt.com/)
  - [DevHub.io](https://devhub.io])
* [code of conduct](http://contributor-covenant.org/)
* [changelog](http://keepachangelog.com/)
* contribution guidelines

When setting up TravisCI and CodeClimate, for a gem supporting Ruby 2.0 and up, the following .travis.yml can be used:

```yaml
language: ruby
os:
- linux
- osx
sudo: false
rvm:
  - 2.0
  - 2.1
  - 2.2
  - 2.3
  - 2.4
  - 2.5
  - ruby-head
matrix:
  allow_failures:
    - rvm: ruby-head
    - rvm: 2.0
      os: osx
    - rvm: 2.4
      os: osx
after_success:
  - bundle exec codeclimate-test-reporter
```

You will also need to add the CODECLIMATE\_REPO\_TOKEN to the TravisCI
environment variables, getting the value from the Code Climate repository
**Settings > Test Coverage**.

Deprecating a project
---------------------

Once a gem is no-longer maintained, it should be clearly marked as such.
Depending upon where the repository is hosted you should set as many of these
points as possible.

* update the title in the README.md to

```markdown
# :no_entry: DEPRECATED project name
```

* add the tags
  - unmaintained
  - deprecated
  - outdated
* mark the repository as archived

Reference
---------

* [shields.io](http://shields.io/) a service for generating more README shields
* https://philna.sh/blog/2017/07/12/two-tests-you-should-run-against-your-ruby-project-now/
* [How I Start - Let's Build a Gem Together! (Book Edition)](https://yukimotopress.github.io/start) by [Steve Klabnik](https://www.steveklabnik.com/)
* [Standard way of marking a github organization or repository as deprecated?](https://stackoverflow.com/questions/44376628/standard-way-of-marking-a-github-organization-or-repository-as-deprecated)

Ruby Blog Entires
=================
* [OPTIONS HTTP Method in Ruby on Rails](/blog/2014-11-26-options-http-method-in-ruby-on-rails)
