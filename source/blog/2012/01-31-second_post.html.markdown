---
title: Second Post
date: 2012-01-31
---
...and its still neglected.

But for now I have an update to my [resume](/resume) and made a small improvement in style.
I have also added a [wiki](/wiki) section, where I am hoping to start putting some of my notes and research that can be public. But we'll start with some recipes.

I still have not put in the time to get [ikiwiki](http://ikiwiki.info) really doing what I want. Either I need to dig in to it, or seriously consider another website platform. [Jekyll](https://github.com/mojombo/jekyll) or [Octopress](http://octopress.org/) might be interesting possibilities.

We'll leave that for another time.
