---
title: Data Furnace
---

A Data Furnace is an idea that was first described by a [paper](http://research.microsoft.com/pubs/150265/heating.pdf)
where computer resources are distributed to homes where the waste heat could be use to warm the home.

# Questions

* what work loads should be included
  - GPU coin mining
  - distributed storage
* initial costs
* energy costs for South-Western Ontario
* expected costs to upgrade hardware over time
* connectivity requirement and availability

# Existing projects or products
* [Neralizer](https://www.nerdalize.com/)
* [Vaper.io Chamber](https://www.vapor.io/chamber/)
* [Quarnot](https://www.qarnot.com/)

# References

* https://en.wikipedia.org/wiki/Data_furnace
* https://news.ycombinator.com/item?id=15620010
* https://www.cloudandheat.com/en/private-infrastructure.html
* [KWLUG November 2017: Large-scale Open Source Storage and Filesharing](https://kwlug.org/node/1105)
* [6 GPU Ethereum Mining Rig Hardware Build Guide - Coin Mining Rigs](http://www.coinminingrigs.com/how-to-build-a-6-gpu-mining-rig/)
* [The era of the cloud’s total dominance is drawing to a close](https://www.economist.com/news/business/21735022-rise-internet-things-one-reason-why-computing-emerging-centralised)
* [Backblaze storage pod specs](https://www.backblaze.com/b2/storage-pod.html)
* [Hyper-converged infrastructure](https://en.wikipedia.org/wiki/Hyper-converged_infrastructure)
  - seems like a PR term, but it might contain some useful ideas
  - the important part seems to be about easily distributing computation
    between the cloud and local hardware
