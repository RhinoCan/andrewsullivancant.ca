---
title: ASC Wiki
---

* [Data Furnace](/wiki/data_furnace)
* [Free/Libre Open Source Software](/wiki/free_libre_open_source_software)
* [Recipes](/wiki/recipes)
* [Remote Work](/wiki/remote_work)
* notes about [Ruby](/wiki/ruby)
* [Waterloo Region Telecom](/wiki/waterloo_region_telecom)

# Research for future projects

* [KW Dashboard](/wiki/kw_dashboard)
